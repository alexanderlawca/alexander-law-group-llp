Alexander Law Group, LLP is an award-winning personal injury law firm with offices in San Jose and San Francisco, California. Providing attorney services for those injured in auto accidents, wrongful death cases, brain & spinal cord injury, burns, defective products and insurance bad faith.

Address: 1 Sansome Street, Suite 3500, San Francisco, CA 94104, USA

Phone: 415-921-1776
